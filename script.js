const transactionsUl =  document.querySelector('#transactions')
const incomeDisplay =   document.querySelector('#money-plus')
const expenseDisplay =  document.querySelector('#money-minus')
const balanceDisplya =  document.querySelector('#balance')
const form =            document.querySelector('#form')
const inputTrasactionName = document.querySelector('#text')
const inputTrasactionAmount = document.querySelector('#amount')

//console.log({inputTrasactionName, inputTrasactionAmount})



const dummyTransactions = [ 
    {id: 1, name: 'Bolo de brigadeiro', amount: -20 },
    {id: 2, name: 'Salario', amount: 300 },
    {id: 3, name: 'Torta de frango', amount: -10 },
    {id: 4, name: 'violão', amount: 150 }
]

const addTrasnsactionIntoDOM = transaction =>{
    const operator = transaction.amount < 0 ? '-' : '+'
    const CSSClas = transaction.amount < 0 ? 'minus' : 'plus'
    const amountWithoutOperator = Math.abs(transaction.amount)
    const li = document.createElement("li")

    li.classList.add(CSSClas)
    li.innerHTML = `
        ${transaction.name} <span>${operator} R$${amountWithoutOperator}</span><button class="delete-btn">x</button>
    `
   transactionsUl.append(li)
}

const updatebalanceValues = ()=>{
    const  trasactionsAmounts = dummyTransactions
        .map(transaction => transaction.amount)
    //soma total
    const total = trasactionsAmounts
        .reduce((accumilator, transaction) => accumilator +transaction, 0)
        .toFixed(2)
    //soma positivos
    const income = trasactionsAmounts
        .filter(value => value > 0)
        .reduce((accumilator, value) => accumilator + value, 0)
        .toFixed(2)
    //soma negativos
    const expense = Math.abs(trasactionsAmounts
        .filter(value => value < 0)
        .reduce((accumulator, value)=> accumulator + value, 0))
        .toFixed(2)
    balanceDisplya.textContent = `R$ ${total}`
    incomeDisplay.textContent = `R$ ${income}`
    expenseDisplay.textContent = `R$ ${expense}`
}

const init = () =>{
    transactionsUl.innerHTML = ''
    dummyTransactions.forEach(addTrasnsactionIntoDOM)
    updatebalanceValues()
}

init ()
const generateID = () => Math.round(Math.random() * 1000)

form.addEventListener('submit', event =>{
    event.preventDefault()
    const transactionName =  inputTrasactionName.value.trim()
    const transactionAmout = inputTrasactionAmount.value.trim()

    if(transactionName === '' || transactionAmout === ''){
        alert('Por favor preencha tanto o nome quanto o valor da transação')
        return
    }
    const transaction = {id: generateID(),
         name:transactionName, 
         amount: Number(transactionAmout)
        }
        
    dummyTransactions.push(transaction)
    init()

    inputTrasactionName.value = ''
    inputTrasactionAmount.value = ''

    })